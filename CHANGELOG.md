# Changelog
## [1.1.21](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.20...v1.1.21) (2024-05-07)

## [1.1.20](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.19...v1.1.20) (2024-05-07)

## [1.1.19](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.18...v1.1.19) (2024-05-07)

## [1.1.18](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.17...v1.1.18) (2024-05-07)

## [1.1.17](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.16...v1.1.17) (2024-05-07)

## [1.1.16](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.15...v1.1.16) (2024-05-07)

## [1.1.15](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.14...v1.1.15) (2024-05-07)

## [1.1.14](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.13...v1.1.14) (2024-05-07)

## [1.1.13](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.12...v1.1.13) (2024-05-07)

## [1.1.12](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.11...v1.1.12) (2024-05-07)

## [1.1.11](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.10...v1.1.11) (2024-05-07)

## [1.1.10](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.9...v1.1.10) (2024-05-07)

## [1.1.9](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.8...v1.1.9) (2024-05-07)

## [1.1.8](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.7...v1.1.8) (2024-05-07)

## [1.1.7](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.6...v1.1.7) (2024-05-07)


### 🐛 Bug Fixes

* package lock ([c915881](https://gitlab.com/rullyafrizal/go-cicd/commit/c9158813f9f54395efdd76d34c05173b04de261b))

## [1.1.2-dev.2](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.1...v1.1.2-dev.2) (2023-10-30)

## [1.1.2-dev.1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.0...v1.1.2-dev.1) (2023-10-30)

## [1.1.2-dev.0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.1-dev.1...v1.1.2-dev.0) (2023-10-30)

## [1.1.6](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.5...v1.1.6) (2024-05-07)

## [1.1.5](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.4...v1.1.5) (2024-05-07)

## [1.1.4](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.3...v1.1.4) (2024-05-07)

## [1.1.3](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2...v1.1.3) (2024-05-07)

## [1.1.2](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.194...v1.1.2) (2024-05-07)

## [1.1.1-dev.1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.1-dev.0...v1.1.1-dev.1) (2023-10-30)

## [1.1.1-dev.0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.11-1...v1.1.1-dev.0) (2023-10-30)

## [1.0.11-1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.11-0...v1.0.11-1) (2023-10-30)

## [1.0.11-0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.9...v1.0.11-0) (2023-10-30)

## [1.0.9](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.8...v1.0.9) (2023-10-30)

## [1.0.8](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.7...v1.0.8) (2023-10-30)

## [1.0.7](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.6...v1.0.7) (2023-10-27)

## [1.0.6](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.5...v1.0.6) (2023-10-27)

## [1.0.5](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.4...v1.0.5) (2023-10-27)

## [1.0.4](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.3...v1.0.4) (2023-10-27)

## [1.0.3](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.2...v1.0.3) (2023-10-27)

## [1.0.2](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.1...v1.0.2) (2023-10-27)

## [1.0.1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.0...v1.0.1) (2023-10-27)

## 1.0.0 (2023-10-27)

## [1.1.2-dev.194](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.193...v1.1.2-dev.194) (2024-05-07)

## [1.1.2-dev.193](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.192...v1.1.2-dev.193) (2024-05-07)

## [1.1.2-dev.192](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.191...v1.1.2-dev.192) (2024-05-07)

## [1.1.2-dev.191](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.190...v1.1.2-dev.191) (2024-05-07)

## [1.1.2-dev.190](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.189...v1.1.2-dev.190) (2024-05-07)

## [1.1.2-dev.189](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.188...v1.1.2-dev.189) (2024-05-07)

## [1.1.2-dev.188](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.187...v1.1.2-dev.188) (2024-05-07)

## [1.1.2-dev.187](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.186...v1.1.2-dev.187) (2024-05-07)

## [1.1.2-dev.186](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.185...v1.1.2-dev.186) (2024-05-07)

## [1.1.2-dev.185](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.184...v1.1.2-dev.185) (2024-05-07)

## [1.1.2-dev.184](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.183...v1.1.2-dev.184) (2024-05-07)

## [1.1.2-dev.183](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.182...v1.1.2-dev.183) (2024-05-07)

## [1.1.2-dev.182](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.181...v1.1.2-dev.182) (2024-05-07)

## [1.1.2-dev.181](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.180...v1.1.2-dev.181) (2024-05-07)

## [1.1.2-dev.180](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.179...v1.1.2-dev.180) (2024-05-07)

## [1.1.2-dev.179](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.178...v1.1.2-dev.179) (2024-05-07)

## [1.1.2-dev.178](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.177...v1.1.2-dev.178) (2024-05-07)

## [1.1.2-dev.177](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.176...v1.1.2-dev.177) (2024-05-07)

## [1.1.2-dev.176](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.175...v1.1.2-dev.176) (2024-05-07)

## [1.1.2-dev.175](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.174...v1.1.2-dev.175) (2024-05-07)

## [1.1.2-dev.174](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.173...v1.1.2-dev.174) (2024-05-07)

## [1.1.2-dev.173](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.172...v1.1.2-dev.173) (2024-05-07)

## [1.1.2-dev.172](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.171...v1.1.2-dev.172) (2024-05-07)

## [1.1.2-dev.171](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.170...v1.1.2-dev.171) (2024-05-07)

## [1.1.2-dev.170](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.169...v1.1.2-dev.170) (2024-05-07)

## [1.1.2-dev.169](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.168...v1.1.2-dev.169) (2024-05-07)

## [1.1.2-dev.168](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.167...v1.1.2-dev.168) (2024-05-07)

## [1.1.2-dev.167](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.166...v1.1.2-dev.167) (2024-05-07)

## [1.1.2-dev.166](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.165...v1.1.2-dev.166) (2024-05-07)

## [1.1.2-dev.165](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.164...v1.1.2-dev.165) (2024-05-07)

## [1.1.2-dev.164](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.163...v1.1.2-dev.164) (2024-05-07)

## [1.1.2-dev.163](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.162...v1.1.2-dev.163) (2024-05-07)

## [1.1.2-dev.162](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.161...v1.1.2-dev.162) (2024-05-07)

## [1.1.2-dev.161](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.160...v1.1.2-dev.161) (2024-05-07)

## [1.1.2-dev.160](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.159...v1.1.2-dev.160) (2024-05-07)

## [1.1.2-dev.159](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.158...v1.1.2-dev.159) (2024-05-07)

## [1.1.2-dev.158](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.157...v1.1.2-dev.158) (2024-05-07)

## [1.1.2-dev.157](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.156...v1.1.2-dev.157) (2024-05-07)

## [1.1.2-dev.156](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.155...v1.1.2-dev.156) (2024-05-07)

## [1.1.2-dev.155](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.154...v1.1.2-dev.155) (2024-05-07)

## [1.1.2-dev.154](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.153...v1.1.2-dev.154) (2024-05-07)

## [1.1.2-dev.153](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.152...v1.1.2-dev.153) (2024-05-07)

## [1.1.2-dev.152](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.151...v1.1.2-dev.152) (2024-05-07)

## [1.1.2-dev.151](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.150...v1.1.2-dev.151) (2024-05-07)

## [1.1.2-dev.150](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.149...v1.1.2-dev.150) (2024-05-07)

## [1.1.2-dev.149](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.148...v1.1.2-dev.149) (2024-05-07)

## [1.1.2-dev.148](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.147...v1.1.2-dev.148) (2024-05-07)

## [1.1.2-dev.147](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.146...v1.1.2-dev.147) (2024-05-07)

## [1.1.2-dev.146](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.145...v1.1.2-dev.146) (2024-05-07)

## [1.1.2-dev.145](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.144...v1.1.2-dev.145) (2024-05-07)

## [1.1.2-dev.144](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.143...v1.1.2-dev.144) (2024-05-07)

## [1.1.2-dev.143](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.142...v1.1.2-dev.143) (2024-05-07)

## [1.1.2-dev.142](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.141...v1.1.2-dev.142) (2024-05-07)

## [1.1.2-dev.141](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.140...v1.1.2-dev.141) (2024-05-07)

## [1.1.2-dev.140](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.139...v1.1.2-dev.140) (2024-05-07)

## [1.1.2-dev.139](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.138...v1.1.2-dev.139) (2024-05-07)

## [1.1.2-dev.138](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.137...v1.1.2-dev.138) (2024-05-07)

## [1.1.2-dev.137](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.136...v1.1.2-dev.137) (2024-05-07)

## [1.1.2-dev.136](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.135...v1.1.2-dev.136) (2024-05-07)

## [1.1.2-dev.135](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.134...v1.1.2-dev.135) (2024-05-07)

## [1.1.2-dev.134](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.133...v1.1.2-dev.134) (2024-05-07)

## [1.1.2-dev.133](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.132...v1.1.2-dev.133) (2024-05-07)

## [1.1.2-dev.132](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.131...v1.1.2-dev.132) (2024-05-07)

## [1.1.2-dev.131](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.130...v1.1.2-dev.131) (2024-05-07)

## [1.1.2-dev.130](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.129...v1.1.2-dev.130) (2024-05-07)

## [1.1.2-dev.129](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.128...v1.1.2-dev.129) (2024-05-07)

## [1.1.2-dev.128](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.127...v1.1.2-dev.128) (2024-05-07)

## [1.1.2-dev.127](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.126...v1.1.2-dev.127) (2024-05-07)

## [1.1.2-dev.126](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.125...v1.1.2-dev.126) (2024-05-07)

## [1.1.2-dev.125](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.124...v1.1.2-dev.125) (2024-05-07)

## [1.1.2-dev.124](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.123...v1.1.2-dev.124) (2024-05-07)

## [1.1.2-dev.123](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.122...v1.1.2-dev.123) (2024-05-07)

## [1.1.2-dev.122](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.121...v1.1.2-dev.122) (2024-05-07)

## [1.1.2-dev.121](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.120...v1.1.2-dev.121) (2024-05-07)

## [1.1.2-dev.120](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.119...v1.1.2-dev.120) (2024-05-07)

## [1.1.2-dev.119](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.118...v1.1.2-dev.119) (2024-05-07)

## [1.1.2-dev.118](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.117...v1.1.2-dev.118) (2024-05-07)

## [1.1.2-dev.117](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.116...v1.1.2-dev.117) (2024-05-07)

## [1.1.2-dev.116](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.115...v1.1.2-dev.116) (2024-05-07)

## [1.1.2-dev.115](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.114...v1.1.2-dev.115) (2024-05-07)

## [1.1.2-dev.114](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.113...v1.1.2-dev.114) (2024-05-07)

## [1.1.2-dev.113](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.112...v1.1.2-dev.113) (2024-05-07)

## [1.1.2-dev.112](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.111...v1.1.2-dev.112) (2024-05-07)

## [1.1.2-dev.111](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.110...v1.1.2-dev.111) (2024-05-07)

## [1.1.2-dev.110](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.109...v1.1.2-dev.110) (2024-05-07)

## [1.1.2-dev.109](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.108...v1.1.2-dev.109) (2024-05-07)

## [1.1.2-dev.108](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.107...v1.1.2-dev.108) (2024-05-07)

## [1.1.2-dev.107](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.106...v1.1.2-dev.107) (2024-05-07)

## [1.1.2-dev.106](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.105...v1.1.2-dev.106) (2024-05-07)

## [1.1.2-dev.105](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.104...v1.1.2-dev.105) (2024-05-07)

## [1.1.2-dev.104](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.103...v1.1.2-dev.104) (2024-05-07)

## [1.1.2-dev.103](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.102...v1.1.2-dev.103) (2024-05-07)

## [1.1.2-dev.102](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.101...v1.1.2-dev.102) (2024-05-07)

## [1.1.2-dev.101](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.100...v1.1.2-dev.101) (2024-05-07)

## [1.1.2-dev.100](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.99...v1.1.2-dev.100) (2024-05-07)

## [1.1.2-dev.99](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.98...v1.1.2-dev.99) (2024-05-07)

## [1.1.2-dev.98](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.97...v1.1.2-dev.98) (2024-05-07)

## [1.1.2-dev.97](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.96...v1.1.2-dev.97) (2024-05-07)

## [1.1.2-dev.96](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.95...v1.1.2-dev.96) (2024-05-07)

## [1.1.2-dev.95](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.94...v1.1.2-dev.95) (2024-05-07)

## [1.1.2-dev.94](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.93...v1.1.2-dev.94) (2024-05-07)

## [1.1.2-dev.93](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.92...v1.1.2-dev.93) (2024-05-07)

## [1.1.2-dev.92](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.91...v1.1.2-dev.92) (2024-05-07)

## [1.1.2-dev.91](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.90...v1.1.2-dev.91) (2024-05-07)

## [1.1.2-dev.90](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.89...v1.1.2-dev.90) (2024-05-07)

## [1.1.2-dev.89](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.88...v1.1.2-dev.89) (2024-05-07)

## [1.1.2-dev.88](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.87...v1.1.2-dev.88) (2024-05-07)

## [1.1.2-dev.87](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.86...v1.1.2-dev.87) (2024-05-07)

## [1.1.2-dev.86](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.85...v1.1.2-dev.86) (2024-05-07)

## [1.1.2-dev.85](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.84...v1.1.2-dev.85) (2024-05-07)

## [1.1.2-dev.84](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.83...v1.1.2-dev.84) (2024-05-07)

## [1.1.2-dev.83](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.82...v1.1.2-dev.83) (2024-05-07)

## [1.1.2-dev.82](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.81...v1.1.2-dev.82) (2024-05-07)

## [1.1.2-dev.81](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.80...v1.1.2-dev.81) (2024-05-07)

## [1.1.2-dev.80](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.79...v1.1.2-dev.80) (2024-05-07)

## [1.1.2-dev.79](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.78...v1.1.2-dev.79) (2024-05-07)

## [1.1.2-dev.78](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.77...v1.1.2-dev.78) (2024-05-07)

## [1.1.2-dev.77](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.76...v1.1.2-dev.77) (2024-05-07)

## [1.1.2-dev.76](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.75...v1.1.2-dev.76) (2024-05-07)

## [1.1.2-dev.75](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.74...v1.1.2-dev.75) (2024-05-07)

## [1.1.2-dev.74](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.73...v1.1.2-dev.74) (2024-05-07)

## [1.1.2-dev.73](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.72...v1.1.2-dev.73) (2024-05-07)

## [1.1.2-dev.72](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.71...v1.1.2-dev.72) (2024-05-07)

## [1.1.2-dev.71](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.70...v1.1.2-dev.71) (2024-05-07)

## [1.1.2-dev.70](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.69...v1.1.2-dev.70) (2024-05-07)

## [1.1.2-dev.69](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.68...v1.1.2-dev.69) (2024-05-07)

## [1.1.2-dev.68](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.67...v1.1.2-dev.68) (2024-05-07)

## [1.1.2-dev.67](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.66...v1.1.2-dev.67) (2024-05-07)

## [1.1.2-dev.66](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.65...v1.1.2-dev.66) (2024-05-07)

## [1.1.2-dev.65](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.64...v1.1.2-dev.65) (2024-05-07)

## [1.1.2-dev.64](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.63...v1.1.2-dev.64) (2024-05-07)

## [1.1.2-dev.63](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.62...v1.1.2-dev.63) (2024-05-07)

## [1.1.2-dev.62](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.61...v1.1.2-dev.62) (2024-05-07)

## [1.1.2-dev.61](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.60...v1.1.2-dev.61) (2024-05-07)

## [1.1.2-dev.60](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.59...v1.1.2-dev.60) (2024-05-07)

## [1.1.2-dev.59](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.58...v1.1.2-dev.59) (2024-05-07)

## [1.1.2-dev.58](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.57...v1.1.2-dev.58) (2024-05-07)

## [1.1.2-dev.57](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.56...v1.1.2-dev.57) (2024-05-07)

## [1.1.2-dev.56](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.55...v1.1.2-dev.56) (2024-05-07)

## [1.1.2-dev.55](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.54...v1.1.2-dev.55) (2024-05-07)

## [1.1.2-dev.54](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.53...v1.1.2-dev.54) (2024-05-07)

## [1.1.2-dev.53](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.52...v1.1.2-dev.53) (2024-05-07)

## [1.1.2-dev.52](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.51...v1.1.2-dev.52) (2024-05-07)

## [1.1.2-dev.51](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.50...v1.1.2-dev.51) (2024-05-07)

## [1.1.2-dev.50](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.49...v1.1.2-dev.50) (2024-05-07)

## [1.1.2-dev.49](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.48...v1.1.2-dev.49) (2024-05-07)

## [1.1.2-dev.48](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.47...v1.1.2-dev.48) (2024-05-07)

## [1.1.2-dev.47](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.46...v1.1.2-dev.47) (2024-05-07)

## [1.1.2-dev.46](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.45...v1.1.2-dev.46) (2024-05-07)

## [1.1.2-dev.45](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.44...v1.1.2-dev.45) (2024-05-07)

## [1.1.2-dev.44](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.43...v1.1.2-dev.44) (2024-05-07)

## [1.1.2-dev.43](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.42...v1.1.2-dev.43) (2024-05-07)

## [1.1.2-dev.42](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.41...v1.1.2-dev.42) (2024-05-07)

## [1.1.2-dev.41](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.40...v1.1.2-dev.41) (2024-05-07)

## [1.1.2-dev.40](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.39...v1.1.2-dev.40) (2024-05-07)

## [1.1.2-dev.39](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.38...v1.1.2-dev.39) (2024-05-07)

## [1.1.2-dev.38](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.37...v1.1.2-dev.38) (2024-05-07)

## [1.1.2-dev.37](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.36...v1.1.2-dev.37) (2024-05-07)

## [1.1.2-dev.36](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.35...v1.1.2-dev.36) (2024-05-07)

## [1.1.2-dev.35](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.34...v1.1.2-dev.35) (2024-05-07)

## [1.1.2-dev.34](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.33...v1.1.2-dev.34) (2024-05-07)

## [1.1.2-dev.33](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.32...v1.1.2-dev.33) (2024-05-07)

## [1.1.2-dev.32](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.31...v1.1.2-dev.32) (2024-05-07)

## [1.1.2-dev.31](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.30...v1.1.2-dev.31) (2024-05-07)

## [1.1.2-dev.30](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.29...v1.1.2-dev.30) (2024-05-07)

## [1.1.2-dev.29](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.28...v1.1.2-dev.29) (2024-05-07)

## [1.1.2-dev.28](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.27...v1.1.2-dev.28) (2024-05-07)

## [1.1.2-dev.27](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.26...v1.1.2-dev.27) (2024-05-07)

## [1.1.2-dev.26](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.25...v1.1.2-dev.26) (2024-05-07)

## [1.1.2-dev.25](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.24...v1.1.2-dev.25) (2024-05-07)

## [1.1.2-dev.24](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.23...v1.1.2-dev.24) (2024-05-07)

## [1.1.2-dev.23](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.22...v1.1.2-dev.23) (2024-05-07)

## [1.1.2-dev.22](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.21...v1.1.2-dev.22) (2024-05-07)

## [1.1.2-dev.21](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.20...v1.1.2-dev.21) (2024-05-07)

## [1.1.2-dev.20](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.19...v1.1.2-dev.20) (2024-05-07)

## [1.1.2-dev.19](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.18...v1.1.2-dev.19) (2024-05-07)

## [1.1.2-dev.18](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.17...v1.1.2-dev.18) (2024-05-07)

## [1.1.2-dev.17](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.16...v1.1.2-dev.17) (2024-05-07)

## [1.1.2-dev.16](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.15...v1.1.2-dev.16) (2024-05-07)

## [1.1.2-dev.15](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.14...v1.1.2-dev.15) (2024-05-07)

## [1.1.2-dev.14](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.13...v1.1.2-dev.14) (2024-05-07)

## [1.1.2-dev.13](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.12...v1.1.2-dev.13) (2024-05-07)

## [1.1.2-dev.12](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.11...v1.1.2-dev.12) (2024-05-07)

## [1.1.2-dev.11](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.10...v1.1.2-dev.11) (2024-05-07)

## [1.1.2-dev.10](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.9...v1.1.2-dev.10) (2024-05-07)

## [1.1.2-dev.9](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.8...v1.1.2-dev.9) (2024-05-07)

## [1.1.2-dev.8](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.7...v1.1.2-dev.8) (2024-05-07)

## [1.1.2-dev.7](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.6...v1.1.2-dev.7) (2024-05-07)

## [1.1.2-dev.6](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.2...v1.1.2-dev.6) (2024-05-07)


### 🐛 Bug Fixes

* package lock ([c915881](https://gitlab.com/rullyafrizal/go-cicd/commit/c9158813f9f54395efdd76d34c05173b04de261b))

## [1.1.2-dev.5](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.4...v1.1.2-dev.5) (2024-05-07)

## [1.1.2-dev.4](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.2...v1.1.2-dev.4) (2024-05-07)


### 🐛 Bug Fixes

* package lock ([c915881](https://gitlab.com/rullyafrizal/go-cicd/commit/c9158813f9f54395efdd76d34c05173b04de261b))

### [1.1.2-dev.1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.2-dev.0...v1.1.2-dev.1) (2023-10-30)

### [1.1.2-dev.0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.1-dev.1...v1.1.2-dev.0) (2023-10-30)

### [1.1.1-dev.1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.1.1-dev.0...v1.1.1-dev.1) (2023-10-30)

### [1.1.1-dev.0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.11-1...v1.1.1-dev.0) (2023-10-30)

### [1.0.11-1](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.11-0...v1.0.11-1) (2023-10-30)

### [1.0.11-0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.9...v1.0.11-0) (2023-10-30)

## [1.0.10-feature/bump-version.0](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.9...v1.0.10-feature/bump-version.0) (2023-10-30)

### [1.0.9](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.8...v1.0.9) (2023-10-30)

### [1.0.8](https://gitlab.com/rullyafrizal/go-cicd/compare/v1.0.7...v1.0.8) (2023-10-30)
