const jsonConfigFile = 'package.json';

const customUpdater = {
  filename: jsonConfigFile,
  type: 'json',
  updater: require('./json-version-updater'),
};

module.exports = {
  header: '# Changelog',
  types: [
    {
      type: 'feat',
      section: '⚡️ Features',
    },
    {
      type: 'fix',
      section: '🐛 Bug Fixes',
    },
  ],
  packageFiles: [customUpdater],
  bumpFiles: [customUpdater],
};